FROM python:3.7-alpine

WORKDIR /app

RUN apk add --no-cache g++ \
                       gcc \
                       bash \
                       make

COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

ADD . /app

