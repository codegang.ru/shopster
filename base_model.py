from typing import Union

from entity import Answer, Question, Item


class BaseModel:
    def __init__(self, items_count: int, questions_count: int, *args, **kwargs):
        self.items_count = items_count
        self.questions_count = questions_count

    def request(self, session_id: str) -> Union[Question, Item]:
        raise NotImplementedError()

    def response(self, session_id: str, answer: Answer) -> None:
        raise NotImplementedError()

    def accept(self, session_id: str) -> None:
        raise NotImplementedError()

    def reject(self, session_id: str) -> None:
        raise NotImplementedError()
