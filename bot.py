import os
import json
import pickle

from flask import Flask, request
import vk_api
from vk_api.keyboard import VkKeyboard, VkKeyboardColor
from vk_api.utils import get_random_id

from entity import Answer, Question, Item
from matrix_model import MatrixModel

app = Flask(__name__)
vk_session = vk_api.VkApi(token=os.environ['CALLBACK_TOKEN'])
vk = vk_session.get_api()

confirmation_code = os.environ['CONFIRMATION_CODE']


with open('questions.txt', 'r') as file:
    questions = [line.strip() for line in file.readlines() if line.strip()]

with open('items.json', 'r') as file:
    items = [f"market{item['owner_id']}_{item['id']}" for item in json.load(file)['response']['items']]

if os.path.exists('model.pkl'):
    with open('model.pkl', 'rb') as file:
        model = pickle.load(file)
else:
    model = MatrixModel(items_count=len(items),
                        questions_count=len(questions))


question_answers = {'yes', 'maybe_yes', 'not_know', 'maybe_no', 'no'}


def default_keyboard():
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button('Начать', color=VkKeyboardColor.PRIMARY, payload={'command': 'start'})
    return keyboard.get_keyboard()


def question_keyboard(session_id: str):
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button('Да', color=VkKeyboardColor.POSITIVE, payload={'command': 'yes', 'session_id': session_id})
    keyboard.add_line()
    keyboard.add_button('Скорее да', color=VkKeyboardColor.POSITIVE, payload={'command': 'maybe_yes', 'session_id': session_id})
    keyboard.add_line()
    keyboard.add_button('Не знаю', color=VkKeyboardColor.DEFAULT, payload={'command': 'not_know', 'session_id': session_id})
    keyboard.add_line()
    keyboard.add_button('Скорее нет', color=VkKeyboardColor.NEGATIVE, payload={'command': 'maybe_no', 'session_id': session_id})
    keyboard.add_line()
    keyboard.add_button('Нет', color=VkKeyboardColor.NEGATIVE, payload={'command': 'no', 'session_id': session_id})
    keyboard.add_line()
    keyboard.add_button('Начать сначала', color=VkKeyboardColor.PRIMARY, payload={'command': 'start'})
    return keyboard.get_keyboard()


def feedback_keyboard(session_id: str):
    keyboard = VkKeyboard(one_time=True)
    keyboard.add_button('Да, спасибо!', color=VkKeyboardColor.POSITIVE, payload={'command': 'accept', 'session_id': session_id})
    keyboard.add_button('Нет, продолжим искать', color=VkKeyboardColor.NEGATIVE, payload={'command': 'reject', 'session_id': session_id})
    keyboard.add_line()
    keyboard.add_button('Начать сначала', color=VkKeyboardColor.PRIMARY, payload={'command': 'start'})
    return keyboard.get_keyboard()


@app.route('/bot', methods=['POST'])
def bot():
    global model

    # получаем данные из запроса
    data = request.get_json(force=True, silent=True)
    # ВКонтакте в своих запросах всегда отправляет поле type:
    if not data or 'type' not in data:
        return 'not ok'

    # проверяем тип пришедшего события
    if data['type'] == 'confirmation':
        # если это запрос защитного кода
        # отправляем его
        return confirmation_code
    # если же это сообщение, отвечаем пользователю
    elif data['type'] == 'message_new':
        # получаем ID пользователя
        from_id = data['object']['message']['from_id']
        payload = data['object']['message'].get('payload')
        if not payload:
            vk.messages.send(message='Не удалось распознать команду. Пожалуйста, используйте только кнопки клавиатуры бота.',
                             random_id=get_random_id(),
                             peer_id=from_id,
                             keyboard=default_keyboard())
            return 'ok'

        payload = json.loads(payload)
        command = payload['command']
        session_id = payload.get('session_id')
        if command == 'start':
            session_id = model.create_session()
            response = model.request(session_id)
            vk.messages.send(message=questions[response.id],
                             random_id=get_random_id(),
                             peer_id=from_id,
                             keyboard=question_keyboard(session_id))
        elif command in question_answers:
            model.response(session_id, Answer[command.upper()])
            response = model.request(session_id)
            if isinstance(response, Question):
                vk.messages.send(message=questions[response.id],
                                 random_id=get_random_id(),
                                 peer_id=from_id,
                                 keyboard=question_keyboard(session_id))
            elif isinstance(response, Item):
                vk.messages.send(message='Возможно, ей понравится это?',
                                 random_id=get_random_id(),
                                 peer_id=from_id,
                                 attachment=items[response.id],
                                 keyboard=feedback_keyboard(session_id))
        elif command == 'accept':
            model.accept(session_id)
            vk.messages.send(message='Всегда рады помочь!',
                             random_id=get_random_id(),
                             peer_id=from_id,
                             keyboard=default_keyboard())
        elif command == 'reject':
            model.reject(session_id)
            response = model.request(session_id)
            vk.messages.send(message=questions[response.id],
                             random_id=get_random_id(),
                             peer_id=from_id,
                             keyboard=question_keyboard(session_id))

    return 'ok'  # игнорируем другие типы