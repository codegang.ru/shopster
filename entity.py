from enum import Enum
from collections import namedtuple


class Answer(Enum):
    YES = 1.
    MAYBE_YES = .5
    NOT_KNOW = 0.
    MAYBE_NO = -.5
    NO = -1

    @staticmethod
    def to_index(answer: 'Answer') -> int:
        for i, item in enumerate(Answer):
            if item == answer:
                return i


class Status(Enum):
    REJECT = False
    ACCEPT = True


Item = namedtuple('Item', ['id'])
Question = namedtuple('Question', ['id'])
