import pickle
from typing import Union
import uuid

from entity import Answer, Status, Question, Item
from base_model import BaseModel
import numpy as np


class MatrixModel(BaseModel):
    MIN_ITEMS = 8
    QUESTION_COUNT = 10
    REPEAT_COUNT = 2

    def __init__(self, items_count: int, questions_count: int, *args, **kwargs):
        super().__init__(items_count, questions_count, *args, **kwargs)
        self._pos_matrix = np.zeros((questions_count, items_count))
        self._neg_matrix = np.zeros((questions_count, items_count))
        self._nk_matrix = np.zeros((questions_count, items_count))

        self._used_question = dict()  # set()
        self._used_items = dict()  # set()
        self._questions = dict()  # []
        self._answers = dict()  # []
        self._current_item = dict()  # None
        self._current_question_count = dict()  # 0
        self._current_matrix = dict()  # np.zeros((self.questions_count, self.items_count))

    def flush(self):
        with open('model.pkl', 'wb') as file:
            pickle.dump(self, file)

    def create_session(self) -> str:
        session_id = str(uuid.uuid4())
        self._questions[session_id] = []
        self._answers[session_id] = []
        self._used_question[session_id] = set()
        self._used_items[session_id] = set()
        self._current_item[session_id] = None
        self._current_question_count[session_id] = 0
        self._current_matrix[session_id] = np.zeros((self.questions_count, self.items_count))
        for i in range(self.questions_count):
            for j in range(self.items_count):
                if (self._pos_matrix[i, j] + self._neg_matrix[i, j]) != 0:
                    self._current_matrix[session_id][i, j] = 2 * self._pos_matrix[i, j] / (self._pos_matrix[i, j] + self._neg_matrix[i, j]) - 1
        self.flush()
        return session_id

    def request(self, session_id: str) -> Union[Question, Item]:
        if len(self._questions[session_id]) % self.QUESTION_COUNT == 0 and len(self._questions[session_id]) > self._current_question_count[session_id] or \
                len(self._questions[session_id]) == (self.REPEAT_COUNT * self.questions_count):

            self._current_question_count[session_id] = len(self._questions[session_id])
            current_vector = np.zeros(self.questions_count)
            for question, answer in zip(self._questions[session_id], self._answers[session_id]):
                current_vector[question] = answer.value

            i = current_vector.dot(self._current_matrix[session_id])
            i_map = sorted(list(zip(i, range(len(i)))), key=lambda x: x[0], reverse=True)
            for value, current_index in i_map:
                if current_index not in self._used_items[session_id]:
                    self._used_items[session_id].add(current_index)
                    self._current_item[session_id] = current_index
                    self.flush()
                    return Item(current_index)

        q = np.zeros(self.questions_count)
        for i in range(self.questions_count):
            pos_row = self._current_matrix[session_id][i, self._current_matrix[session_id][i, :] > 0]
            neg_row = self._current_matrix[session_id][i, self._current_matrix[session_id][i, :] < 0]
            q[i] = np.sum(pos_row) * neg_row.size - np.sum(neg_row) * pos_row.size
        q_map = sorted(list(zip(q, range(len(q)))), key=lambda x: x[0], reverse=True)

        if len(self._used_question[session_id]) == self.questions_count:
            self._used_question[session_id].clear()
        for value, current_index in q_map:
            if current_index not in self._used_question[session_id]:
                self._used_question[session_id].add(current_index)
                self._questions[session_id].append(current_index)
                self.flush()
                return Question(current_index)

    def response(self, session_id: str, answer: Answer) -> None:
        self._answers[session_id].append(answer)
        self.flush()

    def accept(self, session_id: str) -> None:
        if self._current_item[session_id] is not None:
            for question, answer in zip(self._questions[session_id], self._answers[session_id]):
                if answer.value > 0:
                    self._pos_matrix[question, self._current_item[session_id]] += 1
                elif answer.value < 0:
                    self._neg_matrix[question, self._current_item[session_id]] += 1
                else:
                    self._nk_matrix[question, self._current_item[session_id]] += 1
        self.flush()

    def reject(self, session_id: str) -> None:
        self.flush()
